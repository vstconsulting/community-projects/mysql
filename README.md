# Quick MySQL deployment

Project for quick MySQL deployment with [Polemarch](https://github.com/vstconsulting/polemarch). 

Supported OS
------------
This project is able to deploy MySQL on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS.

Project's playbook
------------------

* **deploy.yml**: This playbook deploys MySQL on all hosts from
selected inventory.

Form of quick deployment
------------------------
This project's form of quick deployment provides you with 3 fields:

* **MySQL user home dir** - home directory of user ansible is logging in as - should have root or sudo access.
* **MySQL user name** - name of user ansible is logging in as - should have root or sudo access.
* **MySQL user password** - password of user ansible is logging in as - should have root or sudo access.

Enjoy it!